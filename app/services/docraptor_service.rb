class DocraptorService
  def self.client
    @client ||= DocRaptor::DocApi.new
  end

  attr_accessor :urls, :contents

  def initialize(urls: [])
    self.urls     = urls
  end

  def convert
    urls.map do |url|
      filename = url.split("/").last ? url.split("/").last : url.split("/")[-2]
      t = Tempfile.new(filename, encoding: "ascii-8bit")
      body = self.class.client.create_doc(
        test:             true,
        document_url:     url,
        name:             filename,
        document_type:    "pdf"
      )
      t.write(body)
      t.close
      t
    end
  end

end
