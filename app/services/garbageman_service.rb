class GarbagemanService
  class << self
    def call(file)
      return { message: { error: 'No files to process' }, status: 400 } unless file

      to_send = {
        file:  HTTP::FormData::File.new(file.path),
        token: ENV['GARBAGEMAN_TOKEN']
      }
      handle(response(to_send), file)
    end

    def response(params)
      HTTP.post(ENV['GARBAGEMAN_ENDPOINT'], form: params)
    end

    def handle(response, file)
      if response.code.to_s.start_with? '20'
        response.parse
      else
        { message: { error: 'We had a problem processing this document' }, status: response.code }
      end
    end
  end
end
