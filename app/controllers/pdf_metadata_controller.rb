class PdfMetadataController < ApplicationController
  def index
    if process_params[:urls]
      output = {}
      files = DocraptorService.new(urls: process_params[:urls]).convert
      metadata = files.each_with_index.map{ |f, index| { url: process_params[:urls][index], data: GarbagemanService.call(f) } }
      grouped = metadata.group_by{ |pdf| pdf[:data]["metadata"]["xmpTPg:NPages"].to_i }
      grouped.each do |num, pdfs|
        output[num] = pdfs.sort_by{ |p| p[:url] }.map{ |pdf| prepare_doc(pdf[:url], pdf[:data]) }
      end
      render json: output
    else
      head :unacceptable
    end
  end

  private

  def process_params
    params.permit(urls: [])
  end

  def prepare_doc(url, pdf)
    {
      url: url,
      pdf_version: pdf["metadata"]["pdf:PDFVersion"],
      info: {
        Producer: pdf["metadata"]["producer"],
        Title: pdf["metadata"]["pdf:docinfo:title"]
      },
      metadata: nil,
      page_count: pdf["metadata"]["xmpTPg:NPages"].to_i
    }
  end
end
