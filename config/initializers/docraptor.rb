DocRaptor.configure do |config|
  config.username = ENV["DOCRAPTOR_KEY"] || "YOUR_API_KEY_HERE"
end
