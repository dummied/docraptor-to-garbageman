# DocRaptor to Garbageman

## Running the tests

- `bundle && bundle exec rake`

_NOTE: The integration test makes live HTTP calls and will require working ENV vars to pass_

## Endpoints

- `pdf_metadata` - Takes urls, turns them into pdfs, then turns them
  into text metadata.
    - Params: `urls` (Array)
    - Example call: <https://intense-bastion-46400.herokuapp.com/pdf_metadata?urls[]=http://docraptor.com/examples/invoice.html>
    - Example response:
      ```json
        {"1":[{"url":"http://docraptor.com/examples/invoice.html","pdf_version":"1.5","info":{"Producer":"Prince 12.4 (www.princexml.com)","Title":"Your New Project for Our Best Client"},"metadata":null,"page_count":1}]}
      ```

## Internals

- Controller: `app/controllers/pdf_metadata_controller.rb`
- Services: `app/services/docraptor_service.rb`, `app/services/garbageman_service.rb`

## ENV Vars

- GARBAGEMAN_TOKEN (API token for the [Garbageman](https://www.garbageman.io)
- GARBAGEMAN_ENDPOINT (processing endpoint for Garbageman)
