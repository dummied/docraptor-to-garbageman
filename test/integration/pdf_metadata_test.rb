require 'test_helper'

class PdfMetadataTest < ActionDispatch::IntegrationTest
  test "GET pdf_metadata should return the expected format" do
    get "/pdf_metadata?urls[]=http://docraptor.com/examples/invoice.html"
    assert_equal expected_format, response.body
  end

  private

  def expected_format
    %({"1":[{"url":"http://docraptor.com/examples/invoice.html","pdf_version":"1.5","info":{"Producer":"Prince 12.4 (www.princexml.com)","Title":"Your New Project for Our Best Client"},"metadata":null,"page_count":1}]})
  end
end
