require 'test_helper'

class DocraptorServiceTest < ActiveSupport::TestCase
  def setup
    @sample_url = "http://docraptor.com/examples/invoice.html"
  end

  test "A new service object should have urls, and it should be an array" do
    obj = DocraptorService.new(urls: [ @sample_url ])
    assert obj.urls.is_a? Array
    assert_equal @sample_url, obj.urls.first
  end

  test "the service should return a pdf from a url when requested" do
    result = DocraptorService.new(urls: [ @sample_url ]).convert
    assert result.all?{ |f| f.is_a? Tempfile }, result.map(&:inspect)
    assert result.all?{ |f| f.size > 0 }
  end
end
